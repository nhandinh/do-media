package com.codecamp.media.handler

import com.codecamp.common.dto.Result
import com.codecamp.common.exception.BadRequestException
import com.codecamp.common.exception.FileException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class ApiExceptionHandler {

  @ExceptionHandler(BadRequestException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  Result badRequestException(Exception ex) {
    return new Result().error(ex.message)
  }

  @ExceptionHandler(FileException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  Result notFoundException(Exception ex) {
    return new Result().error(ex.message)
  }
}
