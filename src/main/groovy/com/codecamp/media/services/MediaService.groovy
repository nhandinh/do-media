package com.codecamp.media.services

import com.codecamp.common.domains.Media
import org.springframework.core.io.Resource
import org.springframework.web.multipart.MultipartFile

interface MediaService {

  Media storeFile(MultipartFile file, long userId)

  Resource downloadFile(String fileName)

}