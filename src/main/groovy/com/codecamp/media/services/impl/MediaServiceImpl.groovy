package com.codecamp.media.services.impl

import com.codecamp.common.domains.Media
import com.codecamp.common.domains.User
import com.codecamp.common.exception.FileException
import com.codecamp.media.config.FileStorageProperties
import com.codecamp.media.repositories.MediaRepository
import com.codecamp.media.services.MediaService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.stereotype.Service
import org.springframework.util.StringUtils
import org.springframework.web.multipart.MultipartFile

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.text.Normalizer
import java.util.regex.Pattern

@Service
class MediaServiceImpl implements MediaService {
  static final Pattern NONLATIN = Pattern.compile('[^\\w-]')
  static final Pattern WHITESPACE = Pattern.compile('[\\s]')
  final Path fileStorageLocation

  @Autowired
  MediaRepository mediaRepository

  @Autowired
  MediaServiceImpl(FileStorageProperties fileStorageProperties) {
    this.fileStorageLocation = Paths.get(fileStorageProperties.uploadDir).toAbsolutePath().normalize()
    try {
      Files.createDirectories(this.fileStorageLocation)
    } catch (Exception ex) {
      throw new IllegalAccessException("Can't create folder ${this.fileStorageLocation}")
    }
  }

  @Override
  Media storeFile(MultipartFile file, long userId) {
    String fileName = StringUtils.cleanPath(file.getOriginalFilename())
    try {
      if (fileName.contains('..')) {
        throw new FileException("File name invalid ${fileName}")
      }
      String name = "${UUID.randomUUID().toString()}.${this.getExtensionFile(fileName)}"
      Path targetLocation = this.fileStorageLocation.resolve(name)
      println targetLocation.toAbsolutePath().toString()
      Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING)
      Media media = new Media()
      User user = new User()
      user.id = userId
      media.user = user
      media.fileName = name
      media.slug = this.fileNameToSlug(this.getFileName(fileName))
      this.mediaRepository.save(media)
      return media
    } catch (IOException e) {
      throw new FileException("Can't copy file ${fileName}", e)
    }
  }

  @Override
  Resource downloadFile(String fileName) {
    try {
      Path filePath = this.fileStorageLocation.resolve(fileName).normalize()
      Resource resource = new UrlResource(filePath.toUri());
      if (resource.exists()) {
        return resource
      } else {
        throw new FileException("File not found ${fileName}")
      }
    } catch (MalformedURLException e) {
      throw new FileException("File not found ${fileName}")
    }
  }

  String fileNameToSlug(String input) {
    String noWhitespace = WHITESPACE.matcher(input).replaceAll('-')
    String normalized = Normalizer.normalize(noWhitespace, Normalizer.Form.NFD)
    String slug = NONLATIN.matcher(normalized).replaceAll('')
    return slug.toLowerCase(Locale.ENGLISH)
  }

  String getFileName(String fileName) {
    fileName.lastIndexOf('.').with { it != -1 ? fileName[0..<it] : fileName }
  }

  String getExtensionFile(String fileName) {
    fileName.lastIndexOf('.').with { it != -1 ? fileName.substring(it + 1) : '' }
  }
}
