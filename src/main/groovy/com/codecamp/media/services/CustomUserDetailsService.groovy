package com.codecamp.media.services

import com.codecamp.common.domains.User
import com.codecamp.common.exception.ResourceNotFoundException
import com.codecamp.common.utils.UserPrincipal
import com.codecamp.media.repositories.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class CustomUserDetailsService implements UserDetailsService {

  @Autowired
  UserRepository userRepository

  @Override
  UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = this.userRepository.findByEmail(username)
        .orElseThrow({ -> new UsernameNotFoundException('User not found') })
    UserPrincipal.create(user)
  }

  @Transactional
  UserDetails loadUserById(Long id) {
    User user = userRepository.findById(id).orElseThrow(
        { -> new ResourceNotFoundException('User', 'id', id) }
    )
    UserPrincipal.create(user)
  }
}
