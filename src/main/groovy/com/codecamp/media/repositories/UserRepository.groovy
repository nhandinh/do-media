package com.codecamp.media.repositories

import com.codecamp.common.domains.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository extends JpaRepository<User, Long> {

  Optional<User> findByEmail(String email)

}
