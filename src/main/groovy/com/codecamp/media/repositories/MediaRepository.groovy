package com.codecamp.media.repositories

import com.codecamp.common.domains.Media
import org.springframework.data.jpa.repository.JpaRepository

interface MediaRepository extends JpaRepository<Media, Long> {
}
