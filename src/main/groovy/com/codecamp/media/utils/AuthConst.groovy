package com.codecamp.media.utils

final class AuthConst {

  static final String[] WHITE_LIST_URLS = [
      '/',
      '/error',
      '/favicon.ico'
  ]

  static final String[] APP_PERMIT_ALL = [
  ]
}
