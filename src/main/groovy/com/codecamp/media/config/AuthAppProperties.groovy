package com.codecamp.media.config

import com.codecamp.common.config.AppProperties
import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = 'app')
class AuthAppProperties extends AppProperties {
}
