package com.codecamp.media.config

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = 'file')
class FileStorageProperties {
  String uploadDir
}
