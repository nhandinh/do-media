package com.codecamp.media.config


import com.codecamp.common.auth.RestAuthenticationEntryPoint
import com.codecamp.common.config.AppProperties
import com.codecamp.common.provider.TokenProvider
import com.codecamp.media.filters.TokenAuthenticationFilter
import com.codecamp.media.services.CustomUserDetailsService
import com.codecamp.media.utils.AuthConst
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.BeanIds
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
    securedEnabled = true,
    jsr250Enabled = true,
    prePostEnabled = true
)
class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  CustomUserDetailsService customUserDetailsService

  @Bean
  TokenAuthenticationFilter tokenAuthenticationFilter() {
    return new TokenAuthenticationFilter()
  }

  @Bean
  TokenProvider tokenProvider(AppProperties appProperties) {
    new TokenProvider(appProperties)
  }

  @Override
  void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
    authenticationManagerBuilder
        .userDetailsService(customUserDetailsService)
        .passwordEncoder(passwordEncoder())
  }

  @Bean
  PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder()
  }

  @Bean(BeanIds.AUTHENTICATION_MANAGER)
  @Override
  AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean()
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .cors()
        .and()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .csrf()
        .disable()
        .formLogin()
        .disable()
        .httpBasic()
        .disable()
        .exceptionHandling()
        .authenticationEntryPoint(new RestAuthenticationEntryPoint())
        .and()
        .authorizeRequests()
        .antMatchers(AuthConst.WHITE_LIST_URLS)
        .permitAll()
        .antMatchers(AuthConst.APP_PERMIT_ALL)
        .permitAll()
        .anyRequest()
        .authenticated()

    http.addFilterBefore(tokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
  }
}
