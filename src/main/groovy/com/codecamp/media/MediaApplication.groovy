package com.codecamp.media

import com.codecamp.media.config.AuthAppProperties
import com.codecamp.media.config.FileStorageProperties
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@SpringBootApplication
@EnableEurekaClient
@EntityScan('com.codecamp.common.domains')
@EnableConfigurationProperties([FileStorageProperties.class, AuthAppProperties.class])
class MediaApplication {

  static void main(String[] args) {
    SpringApplication.run MediaApplication, args
  }
}
