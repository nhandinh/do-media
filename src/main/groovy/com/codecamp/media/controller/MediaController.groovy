package com.codecamp.media.controller

import com.codecamp.common.auth.annotation.CurrentUser
import com.codecamp.common.domains.Media
import com.codecamp.common.dto.Result
import com.codecamp.common.utils.UserPrincipal
import com.codecamp.media.services.MediaService
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile

import javax.servlet.http.HttpServletRequest

@RestController
@Slf4j
class MediaController {

  @Autowired
  MediaService mediaService

  @PostMapping(value = '/upload')
  ResponseEntity<Result> uploadFile(@RequestParam('file') MultipartFile file, @CurrentUser UserPrincipal userPrincipal) {
    Media media = this.mediaService.storeFile(file, userPrincipal.id)
    ResponseEntity.ok(new Result<Media>().success(media))
  }

  @GetMapping('/images/{fileName:.+}')
  ResponseEntity<Resource> downloadFile(@PathVariable('fileName')String fileName, HttpServletRequest request) {
    Resource resource = this.mediaService.downloadFile(fileName)
    String contentType = null
    try {
      contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath())
    } catch (IOException ex) {
      log.info('Could not determine file type.')
    }
    if(contentType == null) {
      contentType = 'application/octet-stream'
    }

    return ResponseEntity.ok()
        .contentType(MediaType.parseMediaType(contentType))
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"${resource.filename}\"")
        .body(resource);
  }
}
